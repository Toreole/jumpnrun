﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// The playercontroller from when this was still with a car as the player. please dont look at it.
    /// </summary>
    [System.Obsolete]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        protected Rigidbody2D body;
        [SerializeField]
        protected float maxSpeed;

        protected float xMove;
        protected bool goingHellaFast = false;

        protected bool grounded;
        [SerializeField]
        protected Transform grounder;
        [SerializeField]
        protected LayerMask groundMask;

        [SerializeField]
        protected GameObject explosion;

        [SerializeField]
        protected Transform respawnPoint;
        [SerializeField]
        protected GameObject prefab;

        // Update is called once per frame
        void Update()
        {
            xMove = Input.GetAxis("Horizontal");
        }

        /// <summary>
        /// i said dont look at it
        /// </summary>
        void FixedUpdate()
        {
            grounded = Physics2D.OverlapCircle(grounder.position, 0.2f, groundMask);
            if (grounded)
            {
                body.AddForce(Vector2.right * Mathf.Clamp(xMove * maxSpeed - body.velocity.x, -maxSpeed, maxSpeed));
            }

            if (xMove != 0f)
            {
                goingHellaFast = (Mathf.Abs(body.velocity.x)) > 15f;
            }
        }

        /// <summary>
        /// Stop right there
        /// </summary>
        /// <param name="collision"></param>
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (Mathf.Abs(collision.GetContact(0).normal.x) > 0.75f)
            {
                if (goingHellaFast)
                {
                    Instantiate(explosion, transform.position, Quaternion.identity, null);
                    if (respawnPoint)
                    {
                        var go = Instantiate(prefab, respawnPoint.position, Quaternion.identity, null);
                        go.name = "lmao yeet";
                    }
                    Destroy(gameObject);
                }
            }
        }
    }
}