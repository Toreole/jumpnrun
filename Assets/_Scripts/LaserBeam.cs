﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class LaserBeam : MonoBehaviour
    {
        private LineRenderer lr;

        private Vector2 laserLength;
        private GameObject hitTarget;

        [SerializeField]
        private float LaserRayTime = 2;

        public bool inputPressed;
        public bool laserCanBeFired;


        // Start is called before the first frame update
        void Start()
        {
            lr = GetComponent<LineRenderer>();
            laserCanBeFired = true;
        }


        void LaserRay()
        {
            //Raycast der die Länge der Lasers Festlegt.
            RaycastHit2D hit2D = Physics2D.Raycast(transform.position, new Vector2(1, 0));

            //Get the Object hit By the raycast
            if (hit2D == true)
            {
                hitTarget = hit2D.transform.gameObject;
            }

            //Setzt die Länge des LineRenderes auf die Länge des Raycasts
            laserLength = hit2D.point - (Vector2)transform.position;
            laserLength.y = 0;

            //Wenn der laser Zeit hat zu feuern wir der Laser auf die länge des Raycasts gesetzt
            if (LaserRayTime >= 0)
            {
                lr.SetPositions(new Vector3[] { Vector3.zero, laserLength });
                //lr.SetPosition(1, laserLength);
                LaserRayTime -= Time.deltaTime;

                if (hitTarget.transform.position.x - this.transform.position.x <= 10)
                {

                    if (hitTarget.tag == "CanBeDestroyed")
                    {
                        hitTarget.SetActive(false);
                        lr.SetPositions(new Vector3[] { Vector3.zero, laserLength });
                    }
                }

            }

            //Wenn die Zeit des lasers abgelaufen ist werden die Variablen zurück gesetzt.
            else
            {
                lr.SetPosition(1, new Vector2(0, 0));
                LaserRayTime = 2;
                inputPressed = false;
            }

        }

        // Update is called once per frame
        void Update()
        {

            if (laserCanBeFired == true)
            {
                //Wenn "E" gedrückt wird wir die Kondition das der laser schießt aktiviert
                if (Input.GetKey(KeyCode.E))
                {
                    inputPressed = true;
                    laserCanBeFired = false;
                }
            }
                //Die Aktivations Kondition ruft der Laser auf
                if (inputPressed == true)
                {
                    LaserRay();
                }
        }
    }
}