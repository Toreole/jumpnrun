﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// literally just a note you put in your scene. (editor only)
    /// </summary>
    public class Note : MonoBehaviour
    {
        [TextArea(10, 50)]
        public string text;
    }
}