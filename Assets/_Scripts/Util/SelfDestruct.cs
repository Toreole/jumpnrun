﻿using UnityEngine;
using System.Collections;

namespace CoATwoLA
{
    /// <summary>
    /// currently unused self-destroy script for animated objects
    /// </summary>
    public class SelfDestruct : MonoBehaviour
    {
        [SerializeField]
        protected GameObject root;
        public void Destruct()
        {
            Destroy(root);
        }
    }
}