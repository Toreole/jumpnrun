﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// Helper class for extra methods to reuse and extensions mainly for the player.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// constant for gravity
        /// </summary>
        public const float g = 9.81f;
        /// <summary>
        /// gets the bonus jumping
        /// </summary>
        public static float GetBonusJump(this List<SealModifier> collection)
        {
            float jump = 0f;
            foreach (var x in collection)
                jump += x.JumpBonus;
            return jump;
        }
        /// <summary>
        /// gets the bonus speed from the list
        /// </summary>
        public static float GetBonusSpeed(this List<SealModifier> collection)
        {
            float speed = 0f;
            foreach (var x in collection)
                speed += x.SpeedBonus;
            return speed;
        }
    }
}
