﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// Currently unused script for a Zone that simply moves to the right
    /// </summary>
    public class MovingDeath : MonoBehaviour
    {
        [SerializeField]
        protected Rigidbody2D body;
        private void FixedUpdate()
        {
            body.MovePosition(transform.position + transform.right * Time.deltaTime);
        }
    }
}