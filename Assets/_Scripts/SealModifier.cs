﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace CoATwoLA
{
    /// <summary>
    /// A modifier / powerup that can be added to the player.
    /// </summary>
    [System.Serializable]
    public class SealModifier
    {
        //The stuff you can actually set.
        [SerializeField]
        protected float speedBonus;
        [SerializeField]
        protected float jumpHeightBonus;
        [SerializeField]
        protected Sprite sprite;
        [SerializeField]
        protected float lifeTime;
        [SerializeField]
        protected bool laserRefund;
        
        //runtime stuff that should NOT be serialized since thats not needed anyway
        [System.NonSerialized, HideInInspector] public Image modImage;
        [System.NonSerialized, HideInInspector] public Image timeFill;
        [System.NonSerialized, HideInInspector] public GameObject uiDisplay;

        //oh boi thats a lot of properties
        public float ExpirationTime { get; set; } = 0f;
        public float LifeTime => lifeTime; 
        public float SpeedBonus { get => speedBonus; set => speedBonus = value; }
        public float JumpBonus { get => jumpHeightBonus; set => jumpHeightBonus = value; }
        public Sprite M_Sprite => sprite;
        public bool LaserRefund => laserRefund;
    }
}