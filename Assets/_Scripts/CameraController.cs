﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// A CineMachine inspired Camera Controller that is relatively generic
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        protected Transform followTarget;
        protected Vector2 FollowPosition => followTarget.position;

        [SerializeField, Range(0.01f, 5f)]
        protected float followSpeed = 0.8f;

        [SerializeField]
        protected Vector2 lookAhead = Vector2.zero;

        protected Vector2 predictedVelocity;
        protected Vector2 lastPosition;
        ///<summary>
        ///setup the first position to avoid weird problems at the start of the game
        ///</summary>
        private void Start()
        {
            lastPosition = FollowPosition;
        }

        /// <summary>
        /// Moving in LateUpdate after all movement happened in Update (Interpolation of Rigidbodies also occurs framewise in Update)
        /// </summary>
        private void LateUpdate()
        {
            //save the deltaTime because its a little cleaner
            var timeStep = Time.deltaTime;
            //if the timeStep is quite literally 0, dont do anything because thatd be hella weird
            if (Mathf.Approximately(timeStep, 0))
                return;
            //rough current velocity of the object this camera is following
            predictedVelocity = (lastPosition - FollowPosition) / timeStep;
            //update the position
            lastPosition = FollowPosition;
            //now move the camera to the next position by lerping to the predicted position depending on the lookAhead time.
            transform.position = Vector2.Lerp(transform.position, FollowPosition + Vector2.Scale(lookAhead, predictedVelocity), followSpeed * timeStep);
        }
    }
}