﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoLA
{
    //Handle display for all this stuff
    public class LevelScoreUI : MonoBehaviour
    {
        [SerializeField, Tooltip("The scene name")]
        protected string levelName;
        [SerializeField]
        protected TextMeshProUGUI collectedSeals;
        [SerializeField]
        protected TextMeshProUGUI completionTime;

        public string LevelName => levelName;

        public void UpdateDisplay(HighScoreManager.LevelScore score)
        {
            if (score == null)
                return;
            collectedSeals.text = $"Collected seals: {score.collectedSeals}/3";
            completionTime.text = $"Best time: {score.totalTime.ToString("00.00")}s";
        }

        public void LoadLevel()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
        }
    }
}