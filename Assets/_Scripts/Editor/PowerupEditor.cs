﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CoATwoLA.Edit
{
    [CustomEditor(typeof(Powerup))]
    public class PowerupEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var powerup = target as Powerup;
            var bufferTime = EditorGUILayout.FloatField("Respawn Time", powerup.RespawnTime);
            powerup.RespawnTime = Mathf.Clamp(bufferTime, powerup.Modifier.LifeTime, 60f);
        }
    }
}