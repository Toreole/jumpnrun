﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    /// <summary>
    /// Everything for the pause menu, but mainly Buttons
    /// </summary>
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField]
        protected string mainMenu = "mainMenu";

        /// <summary>
        /// Goes back to the main menu and resets the timescale (since it will inevitably be 0 when the game is paused
        /// </summary>
        public void BackToMenu()
        {
            SceneManager.LoadScene(mainMenu);
            Time.timeScale = 1f;
        }
        
        /// <summary>
        /// Quits the application
        /// </summary>
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}