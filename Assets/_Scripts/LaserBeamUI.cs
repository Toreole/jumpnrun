﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoLA
{
    public class LaserBeamUI : MonoBehaviour
    {

        private GameObject LaserBeam;
        private TextMeshPro textmeshPro;

        // Start is called before the first frame update
        void Start()
        {
            LaserBeam = GameObject.Find("LaserBeamLine");
            textmeshPro = GetComponent<TextMeshPro>();
        }

        void Update()
        {
            if (LaserBeam.GetComponent<LaserBeam>().laserCanBeFired == true)
                textmeshPro.text = "Press (E) to fire your Laser  Charges(1/1)";

            if (LaserBeam.GetComponent<LaserBeam>().laserCanBeFired == false)

                textmeshPro.text = "Press (E) to fire your Laser  Charges(0/1)";
        }
    }
}