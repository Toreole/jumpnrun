﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CoATwoLA
{
    /// <summary>
    /// A Physics based elevator, and yes it can only go up and down.
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
    public class Elevator : MonoBehaviour
    {
        [SerializeField]
        protected Rigidbody2D body;
        [SerializeField]
        protected Vector3 topPosition;
        [SerializeField]
        protected float speed;
        [SerializeField, Tooltip("the time in seconds between ascending and descending")]
        protected float waitTime;
        [SerializeField]
        protected bool activateOnPlayerEnter = false;
        [SerializeField]
        protected string playerTag = "Player";

        //all the stuff set at runtime
        protected bool active;
        protected Vector2 startPosition;
        protected float totalDistance = 0f;
        protected float traveledDistance = 0f;
        protected bool isGoingUp = true;
        protected bool isWaiting = false;
        protected float waitEndTime = 0f;

        //just a property to make my life easier doing the whole editor stuff. SerializedProperty and those are annoying to work with and i cba to do that for such a small thing
        public Vector2 TopPosistion { get => topPosition; set => topPosition = value; }

        /// <summary>
        /// Initial setup: save startPos, set active if needed and calculate the total distance between start and end.
        /// </summary>
        private void Awake()
        {
            startPosition = transform.position;
            active = !activateOnPlayerEnter;
            totalDistance = topPosition.y - startPosition.y;
        }

        //oh boi thats a ugly FixedUpdate
        /// <summary>
        /// The actual Behaviour of the elevator. FixedUpdate for physics movement as usual
        /// </summary>
        private void FixedUpdate()
        {
            if (!active)
                return;
            if (isWaiting)
            {
                //wait for the time you can continue again
                if (Time.time >= waitEndTime)
                {
                    isWaiting = false;
                    isGoingUp = !isGoingUp;
                }
                //idk why, i prefer if-return over if/else
                return;
            }
            Move();
        }

        /// <summary>
        /// This just prevents FixedUpdate from being cluttered.
        /// </summary>
        private void Move()
        {
            var delta = Time.deltaTime * speed;
            traveledDistance += delta;
            //offset t for Lerp
            var offset = traveledDistance / totalDistance;
            var nextOffset = isGoingUp ? offset : 1 - offset;
            //Move to the next position with physics
            body.MovePosition(Vector2.Lerp(startPosition, topPosition, nextOffset));
            //check if the next position has been reached
            if (offset >= 1)
            {
                isWaiting = true;
                waitEndTime = Time.time + waitTime;
                traveledDistance = 0f;
                //activateOnPlayerEnter elevators wait for the player until they move again.
                if (activateOnPlayerEnter)
                    active = false;
            }
        }

        /// <summary>
        /// activate once the player enters the trigger.
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!activateOnPlayerEnter)
                return;
            if(collision.CompareTag(playerTag))
            {
                active = true;
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Elevator))]
    public class ElevatorEditor : Editor
    {
        /// <summary>
        /// Just a nice little handle in the Editor to set the top position. 
        /// </summary>
        //Much nicer than having to guess where the position is, or having an empty gameObject somewhere that determines that position.
        //oh yeah it syncs with the elevators X so horizontal movement isnt really a thing here.
        private void OnSceneGUI()
        {
            var elevator = target as Elevator;
            var ePos = elevator.transform.position;

            var tempPos = Handles.DoPositionHandle(elevator.TopPosistion, Quaternion.identity);
            tempPos.y = Mathf.Clamp(tempPos.y, ePos.y, 100000f);
            tempPos.x = ePos.x;
            elevator.TopPosistion = tempPos;
            Handles.Label(elevator.TopPosistion, "Top Position");
            Handles.color = Color.black;
            Handles.DrawDottedLine(tempPos, ePos, 5f);
        }
    }
#endif
}