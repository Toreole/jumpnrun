﻿using UnityEngine;
using UnityEngine.UI;

namespace CoATwoLA
{
    /// <summary>
    /// Smol helper thats only used on the Modifier Display UI element prefab.
    /// </summary>
    public class ModifierUI : MonoBehaviour
    {
        public Image modImage;
        public Image timeFill;
    }
}