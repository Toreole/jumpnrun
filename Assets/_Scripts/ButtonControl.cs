﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    /// <summary>
    /// Button Helpers for the main menu.
    /// </summary>
    public class ButtonControl : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            SceneManager.LoadScene(scene);
        }

        public void QuitButton()
        {
            Application.Quit();
        }
    }
}