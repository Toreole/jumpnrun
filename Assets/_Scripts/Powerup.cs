﻿using UnityEngine;
using System.Collections;

namespace CoATwoLA
{
    /// <summary>
    /// Generic Powerup that waits to be collected by the player. Couldve been a IPowerup based system but this was faster to manage to do and im lazy
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    public class Powerup : MonoBehaviour
    {
        [SerializeField]
        protected SealModifier modifier;
        [SerializeField]
        protected string playerTag;
        [SerializeField, HideInInspector]
        protected float respawnTime = 5f;
        [SerializeField]
        protected new SpriteRenderer renderer;
        [SerializeField]
        protected new BoxCollider2D collider;

        public float RespawnTime { get => respawnTime; set => respawnTime = value; }
        public SealModifier Modifier => modifier;

        /// <summary>
        /// when the player "collides" with this powerup, he should get it
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.CompareTag(playerTag))
            {
                collision.GetComponent<SealController>().AddModifier(modifier);
                SetActive(false);
                StartCoroutine(Respawn());
            }
        }

        /// <summary>
        /// Wait some time and then re-enable
        /// </summary>
        /// <returns></returns>
        private IEnumerator Respawn()
        {
            yield return new WaitForSeconds(respawnTime);
            SetActive(true);
        }

        /// <summary>
        /// activates/deactivates the renderer and collider, which is the closest you can get to being disabled but still having Coroutines run.
        /// </summary>
        /// <param name="active"></param>
        private void SetActive(bool active)
        {
            renderer.enabled = active;
            collider.enabled = active;
        }

    }
}