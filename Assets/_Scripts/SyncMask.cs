﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class SyncMask : MonoBehaviour
    {
        [SerializeField]
        protected SpriteMask mask;
        [SerializeField]
        protected SpriteRenderer render;
        [SerializeField]
        protected Transform maskTransform;

        private void LateUpdate()
        {
            mask.sprite = render.sprite;
            maskTransform.localScale = new Vector3(render.flipX ? -1 : 1, 1, 1);
        }

    }
}