﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using static CoATwoLA.Util;

namespace CoATwoLA
{
    /// <summary>
    /// The main Player Controller for Movement
    /// </summary>
    public class SealController : MonoBehaviour
    {
        //oh boi thats a lot of serialized fields
        [Header("Required components")]
        [SerializeField]
        protected new SpriteRenderer renderer;
        [SerializeField]
        protected Rigidbody2D body;
        [SerializeField]
        protected Animator animator;
        [SerializeField]
        protected LaserBeam laser;

        [Header("Grounded Check")]
        [SerializeField]
        protected Transform grounder;
        [SerializeField]
        protected LayerMask groundMask;

        [Header("Movement Settings")]
        [SerializeField]
        protected float speed;
        [SerializeField]
        protected float acceleration = 4f;
        [SerializeField]
        protected float jumpHeight = 2f;

        [Header("Input")]
        [SerializeField]
        protected string horizontalInput;
        [SerializeField]
        protected string verticalInput;
        [SerializeField]
        protected string jumpButton;

        [Header("Animation")]
        [SerializeField]
        protected string movingBool = "IsMoving";
        [SerializeField]
        protected string jumpTrigger = "IsJumping";
        [SerializeField]
        protected string groundedBool = "IsGrounded";

        [Header("UI")]
        [SerializeField]
        protected Transform modifierGrid;
        [SerializeField]
        protected GameObject modifierPrefab;
        [SerializeField]
        protected CanvasGroup deathScreen;
        [SerializeField]
        protected float deathFadeTime = 0.5f;
        [SerializeField]
        protected GameObject pauseMenu;
        [SerializeField]
        protected TextMeshProUGUI timeDisplay;

        [Header("Other")]
        [SerializeField]
        protected string deathZoneTag = "DeathZone";
        [SerializeField]
        protected AudioSource deathSound;
        [SerializeField]
        protected string scoreCollectTag = "SealOfApproval";
        [SerializeField]
        protected string goalTag = "Goal";
        [SerializeField]
        protected LevelCompleteScreen completeScreen;

        //runtime input cache
        protected float xMove, yMove;
        protected bool jump;

        //score tracking
        protected int score = 0;
        protected float timePassed = 0f;
        protected string currentLevel;

        //is the player grounded?
        protected bool isGrounded;

        //active player control?
        protected bool isActive = true;

        //is the pause menu opened?
        protected bool pauseMenuOpen = false;

        //List of active modifiers
        protected List<SealModifier> modifiers = new List<SealModifier>();

        /// <summary>
        /// Just get the scene name to use it later on.
        /// </summary>
        private void Start()
        {
            currentLevel = SceneManager.GetActiveScene().name;
        }

        /// <summary>
        /// Update runs important per-frame operations. Precisely input and time updates.
        /// </summary>
        private void Update()
        {
            OpenClosePauseMenu();
            if (!isActive || pauseMenuOpen)
                return;
            FetchInput();
            UpdateModifiers();

            //update score time.
            timePassed += Time.deltaTime;
            timeDisplay.text = $"Time passed: {timePassed.ToString("00.00")}s";
        }

        void OpenClosePauseMenu()
        {
            //Pause Menu Stuff. Hard coded to work with the Esc key. idc
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                pauseMenuOpen = !pauseMenuOpen;
                Time.timeScale = pauseMenuOpen ? 0f : 1f;
                pauseMenu.SetActive(pauseMenuOpen);
            }
        }

        /// <summary>
        /// Get the current input to operate on
        /// </summary>
        void FetchInput()
        {
            xMove = Input.GetAxis(horizontalInput);
            yMove = Input.GetAxis(verticalInput);
                //buffer jump until next physics frame. otherwise input might not be catched
            jump = Input.GetButtonDown(jumpButton) || jump;
        }

        /// <summary>
        /// Fixed Update handles all physics (movement)
        /// </summary>
        private void FixedUpdate()
        {
            if (!isActive || pauseMenuOpen)
                return;
            CheckGrounded();
            Move();
            Jump();
        }

        /// <summary>
        /// A very basic grounded check with a circle underneath the players body
        /// </summary>
        void CheckGrounded()
        {
            var col = Physics2D.OverlapCircle(grounder.position, 0.1f, groundMask);
            isGrounded = (col) ? !col.isTrigger : false;
            animator.SetBool(groundedBool, isGrounded);
        }

        /// <summary>
        /// Basic movement from left to right or vice versa
        /// </summary>
        void Move()
        {
            //only move if the player is grounded and actively inputting
            if(isGrounded)
            {
                if (Mathf.Abs(xMove) > 0.2f)
                {
                    //the target speed to run with all modifiers applied
                    var targetSpeed = modifiers.GetBonusSpeed() + speed;

                    bool lookRight = xMove >= 0.0f;
                    var xDir = lookRight ? 1 : -1;

                    //Manual velocity change
                    var velocity = new Vector2(xDir * targetSpeed * acceleration, 0)  * Time.deltaTime + body.velocity;
                    velocity.x = Mathf.Clamp(velocity.x, -targetSpeed, targetSpeed);
                    body.velocity = velocity;
                    
                    //update rendering
                    renderer.flipX = lookRight;
                }
            }
            animator.SetBool(movingBool, Mathf.Abs(body.velocity.x) > 0.0f);
        }
        
        /// <summary>
        /// Jump by adding upwards velocity
        /// </summary>
        void Jump()
        {
            if (!jump)
                return;
            //we only want jump cached until the next physics step, not until ground is hit
            jump = false;
            //for now dont do mid-air jumps
            if (!isGrounded)
                return;
            //animate
            animator.SetTrigger(jumpTrigger);
            //add the jumping velocity
            var currentJumpHeight = jumpHeight + modifiers.GetBonusJump();
            float v0 = Mathf.Sqrt(currentJumpHeight * 2 * g);
            body.velocity += Vector2.up * v0;
        }

        /// <summary>
        /// Updating the list of modifiers - removing all the ones that are expired, otherwise updating their UI display
        /// </summary>
        void UpdateModifiers()
        {
            var now = Time.time;
            //go through all the modifiers and check if theyre expired
            for(int i = 0; i < modifiers.Count; i++)
            {
                var mod = modifiers[i];
                var remainTime = mod.ExpirationTime - now;
                if(remainTime < 0f)
                {
                    //remove that garbage 
                    modifiers.Remove(mod);
                    Destroy(mod.uiDisplay);
                    return;
                }
                else
                    //normalized remain time as fill amount.
                    mod.timeFill.fillAmount = remainTime / mod.LifeTime;
            }
        }

        /// <summary>
        /// Adds a modifier to the controller.
        /// </summary>
        /// <param name="mod"></param>
        public void AddModifier(SealModifier mod)
        {
            //only modifiers with a lifetime (not instant changes like laser refund only) should be handled by the full system.
            if (mod.LifeTime > 0.1f)
            {
                //set the correct expiration time for the new modifier
                mod.ExpirationTime = Time.time + mod.LifeTime;
                //instantiate the display prefab and assign it to the modifier
                var instPrefab = Instantiate(modifierPrefab, modifierGrid);
                mod.uiDisplay = instPrefab;

                //set up the display with the correct images and use the helper to set the UI images
                var modUI = instPrefab.GetComponent<ModifierUI>();
                mod.modImage = modUI.modImage;
                mod.modImage.sprite = mod.M_Sprite;
                mod.timeFill = modUI.timeFill;
                //finally add it to the list
                modifiers.Add(mod);
            }

            //refresh the laser if needed
            laser.laserCanBeFired = mod.LaserRefund || laser.laserCanBeFired;
        }

        /// <summary>
        /// If the players enters certain triggers, certain things are supposed to happen...
        /// </summary>
        /// <param name="collision"></param>
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.CompareTag(deathZoneTag) && isActive)
            {
                Die();
            }
            else if (collision.CompareTag(scoreCollectTag))
            {
                //Add to the score and destroy the object
                score++;
                Destroy(collision.gameObject);
            }
            else if (collision.CompareTag(goalTag))
            {
                isActive = false;
                bool isNewHighscore = HighScoreManager.CheckHighscoreAndSave(currentLevel, score, timePassed);
                completeScreen.SetFinal(timePassed, score, isNewHighscore);
            }
        }

        /// <summary>
        /// Make the player stop moving and stop input during this time.
        /// </summary>
        protected void Die()
        {
            StartCoroutine(DoDeathScreen());
            //Destroy(body);
            body.gravityScale = 0;
            body.velocity = Vector2.zero;
            isActive = false;
            deathSound.Play();
        }

        /// <summary>
        /// Fade in the deathscreen canvasgroup
        /// </summary>
        IEnumerator DoDeathScreen()
        {
            for(float t = 0; t < deathFadeTime; t += Time.deltaTime)
            {
                deathScreen.alpha = t / deathFadeTime;
                yield return null;
            }
            deathScreen.interactable = true;
            deathScreen.blocksRaycasts = true;
            //queue restarting the level
            yield return DoRestartLevel();
        }

        /// <summary>
        /// Restart the level by simply reloading it completely.
        /// </summary>
        IEnumerator DoRestartLevel()
        {
            yield return new WaitForSeconds(7f);
            SceneManager.LoadScene(currentLevel);
        }
    }
}