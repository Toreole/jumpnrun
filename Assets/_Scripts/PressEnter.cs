﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    //Bennets epic gamer class to go from the credits back to the menu.
    public class PressEnter : MonoBehaviour
    {
        [SerializeField]
        protected string sceneName;

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.LoadScene(sceneName);
            }
        }
    }
}
