﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoLA
{
    public class LevelCompleteScreen : MonoBehaviour
    {
        [SerializeField]
        protected GameObject screenRoot, newHighscore;
        [SerializeField]
        protected TextMeshProUGUI timeText, scoreText;
        [SerializeField]
        protected string menuScene;
        
        public void SetFinal(float time, int score, bool isBest)
        {
            screenRoot.SetActive(true);
            scoreText.text = $"Seals collected: {score.ToString()}/3";
            timeText.text = $"Your time: {time.ToString("00.00")}s";
            newHighscore.SetActive(isBest);
        }

        public void BackToMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(menuScene);
        }

    }
}