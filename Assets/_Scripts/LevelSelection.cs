﻿using UnityEngine;
using System.Collections;

namespace CoATwoLA
{
    public class LevelSelection : MonoBehaviour
    {
        [SerializeField]
        protected LevelScoreUI[] levels;

        private void Start()
        {
            HighScoreManager.ReloadSave();
            foreach(var level in levels)
            {
                level.UpdateDisplay(HighScoreManager.GetScore(level.LevelName));
            }
        }
    }
}
