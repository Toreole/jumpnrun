﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace CoATwoLA
{
    /// <summary>
    /// THe HighScoreManager handles saving and loading of saves, and keeps track of highscores across all levels.
    /// </summary>
    public static class HighScoreManager
    {
        //the saveFile.
        private const string fileName = "sealedSeals.seal";

        //current scores tracked here.
        private static List<LevelScore> scores;

        /// <summary>
        /// small helper to check if a score for a level already exists in this context
        /// </summary>
        /// <param name="level">name of the level</param>
        private static bool ContainsLevel(string level)
        {
            return GetScore(level) != null;
        }
        /// <summary>
        /// small helper to check if a score for a level already exists in this context, also gives out the score on the level if it exists.
        /// </summary>
        /// <param name="level">name of the level</param>
        /// <param name="score">the last saved score, if any</param>
        /// <returns></returns>
        private static bool ContainsLevel(string level, out LevelScore score)
        {
            score = GetScore(level);
            return score != null;
        }

        /// <summary>
        /// gets the score for a given level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static LevelScore GetScore(string level)
        {
            return scores.Find(x => x.levelName.Equals(level));
        }

        /// <summary>
        /// Check whether a new highscore was made with this attempt, then save the progress to the file.
        /// </summary>
        /// <param name="level">scene name</param>
        /// <param name="seals">amount of collected seals</param>
        /// <param name="time">time it took to complete the level</param>
        /// <returns></returns>
        public static bool CheckHighscoreAndSave(string level, int seals, float time)
        {
            if(ContainsLevel(level, out LevelScore lastScore))
            {
                //on same score, update only when the time is better
                if(seals == lastScore.collectedSeals)
                {
                    //then check time
                    if(time < lastScore.totalTime)
                    {
                        //all conditions met, save the new one.
                        lastScore.Update(seals, time);
                        Save();
                        return true;
                    }
                }
                //seals weigh more into score than time.
                else if (seals > lastScore.collectedSeals)
                {
                    lastScore.Update(seals, time);
                    Save();
                    return true;
                }
                return false;
            }
            //add the new score when the level hasnt been tracked before
            scores.Add(
                new LevelScore()
                {
                    levelName = level,
                    collectedSeals = seals,
                    totalTime = time
                });
            Save();
            return true;
        }

        /// <summary>
        /// Saves the score List to the drive
        /// </summary>
        private static void Save()
        {
            //Check if the file at the path already exists, if so delete it and make a new file.
            var path = Path.Combine(Application.persistentDataPath, fileName);
            if (File.Exists(path))
                File.Delete(path);
            FileStream file = File.Open(path, FileMode.CreateNew);
            //simple serialization
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, scores);
            //flush and close the stream
            file.Flush();
            file.Close();
        }

        /// <summary>
        /// Loads the last save.
        /// </summary>
        public static void ReloadSave()
        {
            var path = Path.Combine(Application.persistentDataPath, fileName);
            //only try to read the file if it exists.
            if(File.Exists(path))
            {
                FileStream file = File.Open(path, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                scores = bf.Deserialize(file) as List<LevelScore>;
                //flush and close the stream
                file.Flush();
                file.Close();
                return;
            }
            //scoreboard should be empty
            scores = new List<LevelScore>();
        }

        [Serializable]
        public class LevelScore
        {
            public string levelName;
            public int collectedSeals;
            public float totalTime;

            public void Update(int seals, float time)
            {
                collectedSeals = seals;
                totalTime = time;
            }
        }
    }
}
